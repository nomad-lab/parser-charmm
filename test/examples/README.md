# Tests:
### Source of Tests:

The files in this directory and its subdirectories are only supplied for test purposes of this charmm parser of NOMAD. 
Any other usage of these files needs an appropriate license from CHARMM [charmm](http://charmm.chemistry.harvard.edu). 

Downloading these files are subject to having the appropriate license from CHARMM. 
Before any download attempt of this package, user should accept that a license is 
already issued by CHARMM.

Please see the CHARMM page for more details on how to obtain a license [charmm](http://charmm.chemistry.harvard.edu).

The tests are taken from the test directory of `charmm` (Free Version 41b2).

### Example Tests:

#### Minimization test
- brbtest: Test run for Newton Raphson and diagonalization.
 
#### Molecular Dynamics tests
- dyntest1: Tests a number of dynamics calculations for TRYPTOPHAN using explicit Hydrogen.
- dyntest2: Tests a number of dynamics calculations for TRYPTOPHAN using explicit Hydrogen (LANGEVIN).
- langtest1: A series of Langevin dynamics tests for BUTANE molecule from 4 extended atoms.
- nose1: H2O test run for Nose-Hoover dynamics.
- tip4ptest: TIP4P test with 125 water molecules
